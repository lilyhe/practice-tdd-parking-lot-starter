package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class StandardParkingBoy {
    private List<ParkingLot> parkingLots = new ArrayList<>();

    public void addParkingLot(ParkingLot parkingLot) {
        this.parkingLots.add(parkingLot);
    }

    public ParkingTicket park(Car car) {
        for (ParkingLot parkingLot: this.parkingLots){
            if (parkingLot.hasPosition()) {
                return parkingLot.park(car);
            }
        }
        throw new NoAvailablePositionException();

    }

    public Car fetch(ParkingTicket ticket) {
        for (ParkingLot parkingLot: this.parkingLots) {
            if (parkingLot.hasCorrespondingCar(ticket)) {
                return parkingLot.fetch(ticket);
            }
        }
        throw new UnrecognizedParkingTicketException();
    }


}
