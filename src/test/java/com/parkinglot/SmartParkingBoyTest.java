package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SmartParkingBoyTest {
    @Test
    void should_return_9_when_park_given_2_parking_lots_with_10_position_and_8_position_and_a_car() {
        //given
        ParkingLot parkingLotA = new ParkingLot(10, 10);
        ParkingLot parkingLotB = new ParkingLot(10, 8);

        Car car = new Car();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();

        smartParkingBoy.addParkingLot(parkingLotA);
        smartParkingBoy.addParkingLot(parkingLotB);

        //when
        ParkingTicket ticket = smartParkingBoy.park(car);

        //then
        assertEquals(9,parkingLotA.getRemainingCapacity());

    }

    @Test
    void should_return_9_when_park_given_2_parking_lots_with_8_position_and_10_position_and_a_car() {
        //given
        ParkingLot parkingLotA = new ParkingLot(10, 8);
        ParkingLot parkingLotB = new ParkingLot(10, 10);

        Car car = new Car();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();

        smartParkingBoy.addParkingLot(parkingLotA);
        smartParkingBoy.addParkingLot(parkingLotB);

        //when
        ParkingTicket ticket = smartParkingBoy.park(car);

        //then
        assertEquals(9,parkingLotB.getRemainingCapacity());

    }

    @Test
    void should_return_right_car_when_fetch_cars_in_2_parking_lots_given_2_tickets() {
        //given
        ParkingLot parkingLotA = new ParkingLot(5, 5);
        ParkingLot parkingLotB = new ParkingLot(10, 10);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        smartParkingBoy.addParkingLot(parkingLotA);
        smartParkingBoy.addParkingLot(parkingLotB);

        Car carA = new Car();
        Car carB = new Car();

        ParkingTicket ticketA = parkingLotA.park(carA);
        ParkingTicket ticketB = parkingLotB.park(carB);

        //when
        Car fetchedCarA = smartParkingBoy.fetch(ticketA);
        Car fetchedCarB = smartParkingBoy.fetch(ticketB);

        //then
        assertEquals(carA, fetchedCarA);
        assertEquals(carB, fetchedCarB);
    }

    @Test
    void should_throw_exception_when_fetch_given_2_parking_lots_and_a_wrong_ticket() {
        //given
        ParkingLot parkingLotA = new ParkingLot(5, 5);
        ParkingLot parkingLotB = new ParkingLot(10, 10);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        smartParkingBoy.addParkingLot(parkingLotA);
        smartParkingBoy.addParkingLot(parkingLotB);

        ParkingTicket wrongTicket = new ParkingTicket();
        Car car = new Car();

        //when
        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> smartParkingBoy.fetch(wrongTicket));

        //then
        assertEquals("Unrecognized Parking Ticket.", exception.getMessage());

    }

    @Test
    void should_throw_exception_when_fetch_given_2_parking_lots_and_a_used_ticket() {
        //given
        ParkingLot parkingLotA = new ParkingLot(5, 5);
        ParkingLot parkingLotB = new ParkingLot(10, 10);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        smartParkingBoy.addParkingLot(parkingLotA);
        smartParkingBoy.addParkingLot(parkingLotB);

        Car car = new Car();

        ParkingTicket usedTicket = smartParkingBoy.park(car);
        smartParkingBoy.fetch(usedTicket);

        //when
        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> smartParkingBoy.fetch(usedTicket));

        //then
        assertEquals("Unrecognized Parking Ticket.", exception.getMessage());

    }

    @Test
    void should_throw_exception_when_park_given_2_parking_lots_both_without_position() {
        //given
        ParkingLot parkingLotA = new ParkingLot(10, 0);
        ParkingLot parkingLotB = new ParkingLot(10, 0);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        smartParkingBoy.addParkingLot(parkingLotA);
        smartParkingBoy.addParkingLot(parkingLotB);

        Car car = new Car();

        //when
        var exception = assertThrows(NoAvailablePositionException.class, () -> smartParkingBoy.park(car));

        //then
        assertEquals("No available position.", exception.getMessage());

    }

}
